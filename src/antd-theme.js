
import { SkTheme } from "../../sk-core/src/sk-theme.js";


export class AntdTheme extends SkTheme {

    get basePath() {
        if (! this._basePath) {
            this._basePath = (this.configEl && typeof this.configEl.hasAttribute === 'function'
                && this.configEl.hasAttribute('theme-path'))
                ? `${this.configEl.getAttribute('theme-path')}` : '/node_modules/sk-theme-antd';
        }
        return this._basePath;
    }

    get styles() {
        if (! this._styles) {
            this._styles = {'antd.css': `${this.basePath}/antd.min.css`, 'antd-theme.css': `${this.basePath}/antd-theme.css`};
        }
        return this._styles;
    }

}